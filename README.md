Project shows different approaches for creating resource server with standard protocol Oauth2.0

this project contains subjects:

 - [simple resource server with @EnableResourceServer annotation from Oauth2.0 library](https://bitbucket.org/folusz-sebastian/resourceserver/src/resourceServerOldImplement/)
 - [simple resource server with configuration from spring security library](https://bitbucket.org/folusz-sebastian/resourceserver/src/resourceServerNewImplement/)
 - [token store](https://bitbucket.org/folusz-sebastian/resourceserver/src/resourceServerOldImplWithJDBC/)
 - [JWT symmetric key](https://bitbucket.org/folusz-sebastian/resourceserver/src/JwtSymmetricKey/)
 - [JWT asymmetric key](https://bitbucket.org/folusz-sebastian/resourceserver/src/jwtAsymmetricKey/)
 - [JWT asymmetric key only on authorization server side](https://bitbucket.org/folusz-sebastian/resourceserver/src/jwtAsymmetricKeyWithKeysOnlyOnAuthSide/)

Project was created based on "Spring Security in Action" book written by Laurențiu Spilcă.
